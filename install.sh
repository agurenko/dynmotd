#!/bin/bash
clear

WD=$(dirname "$(readlink -f "$0")")

if [ -e '/usr/local/bin/dynmotd' ]; then
    while test $# -gt 0
    do
        if [ $1 == '-f' ]; then
            printf "Overwritting dynmotd with new current version..."
            cp -f $WD/dynmotd /usr/local/bin/dynmotd
            echo "Done"
        else
            echo "Exisiting file found. No changes made"
        fi
        shift
    done
else
    printf "Copying configuration to a new location..."
    cp $WD/dynmotd /usr/local/bin/dynmotd
    if [ -e '/usr/local/bin/dynmotd' ]; then
        echo "Done"
    else
        echo "Error"
        exit
    fi
fi

if [ -e '/usr/local/bin/dynmotd' ]; then
    printf "Changing permissions..."
    chmod 755 /usr/local/bin/dynmotd
    if [ $(stat -c %a /usr/local/bin/dynmotd) == '755' ]; then
        echo "Done"
    else
        echo "Error"
        echo "Please set permissions for /usr/local/bin/dynmotd to 755 manually!"
    fi
else
    echo "File does not exist"
    exit
fi

printf "Adding new PAM module..."
if [[ ! $(grep -i "# session optional pam_motd.so" /etc/pam.d/login) ]]; then
    echo "# session optional pam_motd.so" >> /etc/pam.d/login
    if [[ $(grep -i "# session optional pam_motd.so" /etc/pam.d/login) ]]; then
        echo "Done"
    else
        echo "Error"
    fi
else
    echo "Skipped"
fi

printf "Adding dynmotd to profile..."
if [[ ! $(grep -i "/usr/local/bin/dynmotd" /etc/profile) ]]; then
    echo "/usr/local/bin/dynmotd" >> /etc/profile
    if [[ $(grep -i "/usr/local/bin/dynmotd" /etc/profile) ]]; then
        echo "Done"
    else
        echo "Error"
    fi
else
    echo "Skipped"
fi

printf "Disabling SSH motd..."
if [[ $(grep -i PrintMotd /etc/ssh/sshd_config | awk '{print $2}') == 'yes' ]]; then
    sed -i 's/PrintMotd yes/PrintMotd no/g' /etc/ssh/sshd_config
    if [[ ! $(grep -i PrintMotd /etc/ssh/sshd_config | awk '{print $2}') == 'yes' ]]; then
        echo "Done"
    else
        echo "Error"
    fi
else
    echo "Skipped"
fi

printf "Disabling /etc/motd..."
if [ -e '/etc/motd' ]; then
    mv /etc/motd /etc/motd.disabled
    if [ ! -e '/etc/motd' ]; then
        echo "Done"
    else
        echo "Error"
    fi
else
    echo "Skipped"
fi
