Prints informative MOTD for a quick overview of the system.

Example:

```Good afternoon, root

⌜=================================== HOST ====================================⌝
|
|- Hostname............: hostname.local
|- Kernel..............: 3.10.0-1062.4.3.el7.x86_64
|- Release.............: Red Hat Enterprise Linux Server release 7.7 (Maipo)
|- Time & Date.........: Fri Nov 29 16:44:05 CET 2019
|- Time zone...........: Europe/Berlin (NTP Sync: yes)
|- Users...............: Currently 2 user(s) logged on
|
|=================================== SYSTEM ==================================|
|
|- Current user........: root
|- CPU.................: Intel(R) Xeon(R) CPU E5-2630 v4 @ 2.20GHz (40 threads)
|- CPU usage...........: 5.90, 3.65, 3.62 (1, 5, 15 min)
|- Memory used.........: 140611 MB / 257563 MB
|- Swap in use.........: 0 MB / 4095 MB
|- Processes...........: 444 running
|- System uptime.......: 9 days 3 hours 51 minutes 41 seconds
|- Free space / .......: 648G remaining
|
|=================================== KVM =====================================|
| ID    Name            Status
|-----------------------------------------------------------------------------|
| 15    worker-3        running
| 28    master-0        running
| 29    master-1        running
| 30    master-2        running
| 41    worker-0        running
| 43    worker-2        running
| 47    worker-1        running
⌜=================================== OpenShift ===============================⌝
| ph - connect to the provisionhost VM
|=================================== TMUX ====================================|
| 0: 1 windows (created Mon Nov 25 16:53:25 2019) [210x53]
| 2: 1 windows (created Fri Nov 29 16:11:58 2019) [211x54] (attached)
|=============================================================================|
| tmux [new] / tmux new -s <name>      - new session
| tmux a / tmux attach -t <session>    - restore/share session
| tmux kill-session -t <session>       - kill session
|=================================== UTILS ===================================|
| snapshot <create|revert|update|delete|list> [snapshot_name] [--running]
⌞=============================================================================⌟```