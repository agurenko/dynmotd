#!/bin/bash
clear # I'm Alpha and Omega, no one prints before me, mu-ha-ha

# Formatting setup
BOLD=$(tput bold)
NORM=$(tput sgr0)
# Colours setup
RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
NC='\033[0m'

if [ "$EUID" -eq 0 ]; then
    virsh_command="virsh"
elif [[ $(groups | grep wheel) ]]; then
    virsh_command="sudo virsh"
else
    NO_VIRSH=true
fi

function evaluate_env {

    if [[ ! $(rpm -q libvirt | grep "not installed") || ! $(rpm -q libvirt-client | grep "not installed") ]]; then
        if [[ ! $(systemctl status libvirtd | grep Active | grep dead) ]]; then
            if [[ ! ${NO_VIRSH} ]]; then
                export HAS_LIBVIRT=true
                
                if [[ $(${virsh_command} list --all | grep -i undercloud) ]]; then
                    export IS_VIRT_OSP=true
                fi

                if [[ $(${virsh_command} list --all | grep -i master) ]]; then
                    export IS_VIRT_OCP=true
                fi
            else
                export HAS_LIBVIRT=false
            fi
        fi
    fi

    if [[ $(command oc) ]]; then
        if [[ $(whoami) != "root" ]]; then
            export IS_BM_OCP=true
        fi
    fi

    if [ ! $(rpm -q tmux | grep "not installed") ]; then
        export HAS_TMUX=true
    fi

    if [ -d /root/server-tools ]; then
        export HAS_UTILS=true
    fi
}

function print_virsh {

    echo "|=================================== KVM ======================================|"
    echo -e "|  ID\tName\t\t    Status"
    echo "|------------------------------------------------------------------------------|"
    ${virsh_command} list --all | tail -n+3 | head -n-1 | awk '{print "| " $0}'

}

function print_tmux {
echo "|=================================== TMUX =====================================|"
if [[ $(tmux ls 2>&1 | grep "error connecting") || $(tmux ls 2>&1 | grep "no server running") ]]; then
    echo "| No active tmux sessions"
else
    tmux ls | awk '{print "| "$0}'
fi
echo "|==============================================================================|"
echo "| ${BOLD}tmux${NORM}    or  ${BOLD}tmux new -s <name>${NORM}          - new session"
echo "| ${BOLD}tmux a${NORM}  or  ${BOLD}tmux attach -t <session>${NORM}    - restore/share session"
echo "| ${BOLD}tmux kill-session -t <session>${NORM}          - kill session"
}

function print_utils {

    echo "|=================================== UTILS ====================================|"
    echo "| snapshot <create|revert|update|delete|list> [snapshot_name] [--running]"

}

function print_openstack {

    echo "⌜=================================== OpenStack ================================⌝"

    if [[ $(${virsh_command} list --all | grep undercloud) && $(${virsh_command} list --all | grep undercloud | awk '{print $3}') == "running" ]]; then
        uc_host=$(cat /etc/hosts | grep undercloud | awk '{print $1}')

        sed -i '/undercloud/d' .ssh/known_hosts
        ssh-keyscan ${uc_host} 2>/dev/null >> .ssh/known_hosts

        ssh stack@${uc_host} "if [ ! -f /etc/rhosp-release ]; then sudo yum install -y rhosp-release; fi" > /dev/null

        printf "| "
        echo "| $(ssh stack@${uc_host} 'cat /etc/rhosp-release')"
        printf "| "
        echo "| Puddle: $(ssh stack@${uc_host} 'cat /home/stack/core_puddle_version')"
        echo "| uc - connect to existing undercloud"
    else
        echo "| Undercloud not found or inaccessible"
    fi

}

function print_openshift {

    if [ $IS_VIRT_OCP ]; then
        echo "⌜=================================== OpenShift (virt) =========================⌝"
    
        phhost=$(${virsh_command} list --all | grep provisionhost | awk '{print $2}')
        phhost_status=$(${virsh_command} list --all | grep provisionhost | awk '{print $3}')
    
        if [[ $phhost_status == "running" ]]; then
            printf "| Cluster Version: "
            ssh kni@ph "export KUBECONFIG=/home/kni/clusterconfigs/auth/kubeconfig; oc get clusterversion -o wide" 2>/dev/null | awk 'FNR==2{print $2}'
            if [[ ${PIPESTATUS[0]} -eq 1 ]]; then
                printf "Unknown"
            fi
            echo
            echo "|"
            echo "| ${BOLD}ph${NORM} - connect to the provisionhost VM"
        else
            echo "| Provisionhost machine is not running"
        fi
    elif [ $IS_BM_OCP ]; then
        echo "⌜=================================== OpenShift (BM) ===========================⌝"
        if [ -f "/home/kni/clusterconfigs/auth/kubeconfig" ]; then
            printf "| Cluster Version: "
            KUBECONFIG=/home/kni/clusterconfigs/auth/kubeconfig oc get clusterversion -o wide 2>/dev/null | awk 'FNR==2{print $2}'
            if [[ ${PIPESTATUS[0]} -eq 1 ]]; then
                printf "Unknown"
                echo
            fi
        else
            echo "No KUBECONFIG file found"
        fi
    fi

}

# User section
USER=`whoami`
if [ $USER == 'root' ]; then
    USER=${RED}${USER}${NC}
else
    USER=${GREEN}${USER}${NC}
fi

HOSTNAME=`uname -n`
ROOT=`df -Ph | grep -w / | awk '{print $4}' | tr -d '\n'`
HOME=`df -Ph | grep home | awk '{print $4}' | tr -d '\n'`
BACKUP=`df -Ph | grep backup | awk '{print $4}' | tr -d '\n'`

TIME_ZONE=$(timedatectl | grep "Time zone" | awk '{print $3}')
NTP=$(timedatectl | grep "NTP" | awk '{print $3}')

CPU_MODEL=$(cat /proc/cpuinfo | grep "model name" | head -n1 | awk '{$1=$2=$3=""; print $0}' | xargs)
CPU_THREADS=$(cat /proc/cpuinfo | grep processor | wc -l)

MEMORY1=`free -m | grep Mem | awk '{print $3" MB";}'`
MEMORY2=`free -m | grep "Mem" | awk '{print $2" MB";}'`
PSA=`ps -Afl | wc -l`
SWAP=`free -m | tail -n 1 | awk '{print $3}'`
SWAP_TOTAL=$(free -m | tail -n 1 | awk '{print $2}')

# time of day
HOUR=$(date +"%H")
if [ $HOUR -lt 3  -a $HOUR -ge 0 ]; then
    TIME="night"
elif [ $HOUR -lt 12  -a $HOUR -ge 6 ]; then
    TIME="morning"
elif [ $HOUR -lt 18 -a $HOUR -ge 12 ]; then
    TIME="afternoon"
else
    TIME="evening"
fi

#System uptime
uptime=`cat /proc/uptime | cut -f1 -d.`
upDays=$((uptime/60/60/24))
upHours=$((uptime/60/60%24))
upMins=$((uptime/60%60))
upSecs=$((uptime%60))

#System load
LOAD1=`cat /proc/loadavg | awk {'print $1'}`
LOAD5=`cat /proc/loadavg | awk {'print $2'}`
LOAD15=`cat /proc/loadavg | awk {'print $3'}`

echo -e "Good $TIME, $USER"
echo -e "$STY
⌜=================================== HOST =====================================⌝
|
| Hostname............: $HOSTNAME
| Kernel..............: `uname -r`
| Release.............: `cat /etc/redhat-release`
| Time & Date.........: `date`
| Time zone...........: $TIME_ZONE (NTP Sync: $NTP)
| Users...............: Currently `users | wc -w` user(s) logged on
|
|==================================== SYSTEM ==================================|
|
| Current user........: $USER
| CPU.................: $CPU_MODEL ($CPU_THREADS threads)
| CPU usage...........: $LOAD1, $LOAD5, $LOAD15 (1, 5, 15 min)
| Memory used.........: $MEMORY1 / $MEMORY2
| Swap in use.........: $SWAP MB / $SWAP_TOTAL MB
| Processes...........: $PSA running
| System uptime.......: $upDays days $upHours hours $upMins minutes $upSecs seconds
| Free space / .......: $ROOT remaining
|"

evaluate_env

if [ $HAS_LIBVIRT ]; then print_virsh; fi

if [ $IS_VIRT_OSP ]; then print_openstack; fi

if [[ $IS_VIRT_OCP || $IS_BM_OCP ]]; then print_openshift; fi

if [ $HAS_TMUX ]; then print_tmux; fi

if [ $HAS_UTILS ]; then print_utils; fi

echo "⌞==============================================================================⌟"
echo
